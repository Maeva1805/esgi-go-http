package main

import (
	"fmt"
	"net/http"
	"time"
	"html"
	"os"
	"bufio"
)

func main() {

	//routes
	http.HandleFunc("/", timeHandler)
	http.HandleFunc("/add", addHandler)
	http.HandleFunc("/entries", entryHandler)

	http.ListenAndServe(":4567", nil)
}


// ROUTES

func timeHandler(w http.ResponseWriter, req *http.Request) {

	switch req.Method {

	case http.MethodGet:	// GET
		fmt.Fprintf(w, "GET %q\n", html.EscapeString(req.URL.Path))
		var now = time.Now()
		fmt.Fprintf(w, "> %02vh%02v\n", now.Hour(), now.Minute())

	}
}

func addHandler(w http.ResponseWriter, req *http.Request) {

	switch req.Method {

	case http.MethodPost:	// POST
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}

		saveEntry(req.FormValue("author"), req.FormValue("entry"))

		fmt.Fprintf(w, "POST %q\n", html.EscapeString(req.URL.Path))
		fmt.Fprintf(w, "%v: %v\n", req.FormValue("author"), req.FormValue("entry"))
	
	}
}

func entryHandler(w http.ResponseWriter, req *http.Request) {

	switch req.Method {

	case http.MethodGet:	// GET
		fmt.Fprintf(w, "GET %q\n", html.EscapeString(req.URL.Path))
		fmt.Fprintln(w, readEntries())

	}
}


// FUNCTIONS

func readEntries() string {
	saveData, err := os.ReadFile("./entries.data")
	if err == nil {
		return string(saveData)
	}
	return ""
}

func saveEntry(nextAuthor, nextEntry string) {

	saveFile, err := os.OpenFile("./entries.data", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	defer saveFile.Close()

	//write file
	w := bufio.NewWriter(saveFile)
	if err == nil {
		fmt.Fprintf(w, "%s: %s\n", nextAuthor, nextEntry)
	}
	w.Flush()
}